//
//  ViewController.m
//  BLE Shield Controller
//
//  Created by zaker-7 on 13-4-25.
//  Copyright (c) 2013年 zaker-7. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize ble = _ble;

- (void)dealloc
{
    _ble.delegate = nil;

}

- (void)setBle:(BLE *)ble
{
    _ble = ble;
}

- (BLE *)ble
{
    if (!_ble) {
        _ble = [[BLE alloc] init];
        [_ble controlSetup:1];
        _ble.delegate = self;
    }
    
    return _ble;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
