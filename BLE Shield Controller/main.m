//
//  main.m
//  BLE Shield Controller
//
//  Created by zaker-7 on 13-4-25.
//  Copyright (c) 2013年 zaker-7. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
