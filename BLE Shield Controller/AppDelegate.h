//
//  AppDelegate.h
//  BLE Shield Controller
//
//  Created by zaker-7 on 13-4-25.
//  Copyright (c) 2013年 zaker-7. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
