//
//  ViewController.h
//  BLE Shield Controller
//
//  Created by zaker-7 on 13-4-25.
//  Copyright (c) 2013年 zaker-7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BLE.h"

@interface ViewController : UIViewController<BLEDelegate>

@property (strong, nonatomic) BLE *ble;

@end
